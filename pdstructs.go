package main

import (
	"time"
)

type Services struct {
	Services []struct {
		ID                     string        `json:"id"`
		Name                   string        `json:"name"`
		Description            string        `json:"description"`
		AutoResolveTimeout     int           `json:"auto_resolve_timeout"`
		AcknowledgementTimeout int           `json:"acknowledgement_timeout"`
		CreatedAt              time.Time     `json:"created_at"`
		Status                 string        `json:"status"`
		LastIncidentTimestamp  interface{}   `json:"last_incident_timestamp"`
		Teams                  []interface{} `json:"teams"`
		IncidentUrgencyRule    struct {
			Type    string `json:"type"`
			Urgency string `json:"urgency"`
		} `json:"incident_urgency_rule"`
		ScheduledActions []interface{} `json:"scheduled_actions"`
		SupportHours     interface{}   `json:"support_hours"`
		EscalationPolicy struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"escalation_policy"`
		Addons        []interface{} `json:"addons"`
		Privilege     interface{}   `json:"privilege"`
		AlertCreation string        `json:"alert_creation"`
		Integrations  []struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"integrations"`
		Type    string `json:"type"`
		Summary string `json:"summary"`
		Self    string `json:"self"`
		HTMLURL string `json:"html_url"`
	} `json:"services"`
	Limit  int         `json:"limit"`
	Offset int         `json:"offset"`
	Total  interface{} `json:"total"`
	More   bool        `json:"more"`
}

/*

type LogData struct {
	LogEntry struct {
		ID      string `json:"id"`
		Channel struct {
			Type        string            `json:"type"`
			Description string            `json:"description"`
			Details     map[string]string `json:"details,omitempty"`
		} `json:"channel"`
		Contexts []interface{} `json:"contexts"`
	} `json:"log_entry"`
}

*/
/*
type Data struct {
	Incidents []struct {
		IncidentNumber     int       `json:"incident_number"`
		Title              string    `json:"title"`
		Description        string    `json:"description"`
		CreatedAt          time.Time `json:"created_at"`
		Status             string    `json:"status"`
		IncidentKey        string    `json:"incident_key"`
		LastStatusChangeAt time.Time `json:"last_status_change_at"`
		Urgency            string    `json:"urgency"`
		ID                 string    `json:"id"`
		Summary            string    `json:"summary"`
		Self               string    `json:"self"`
		HTMLURL            string    `json:"html_url"`
	} `json:"incidents"`
	Limit  int  `json:"limit"`
	Offset int  `json:"offset"`
	More   bool `json:"more"`
}
*/

/*
type CompleteIncident struct {
	Incident struct {
		IncidentNumber int           `json:"incident_number"`
		Title          string        `json:"title"`
		Description    string        `json:"description"`
		CreatedAt      time.Time     `json:"created_at"`
		Status         string        `json:"status"`
		PendingActions []interface{} `json:"pending_actions"`
		IncidentKey    string        `json:"incident_key"`
		Service        struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"service"`
		Assignments        []interface{} `json:"assignments"`
		Acknowledgements   []interface{} `json:"acknowledgements"`
		LastStatusChangeAt time.Time     `json:"last_status_change_at"`
		LastStatusChangeBy struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"last_status_change_by"`
		FirstTriggerLogEntry struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"first_trigger_log_entry"`
		EscalationPolicy struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"escalation_policy"`
		Privilege interface{} `json:"privilege"`
		Teams     []struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"teams"`
		AlertCounts struct {
			All       int `json:"all"`
			Triggered int `json:"triggered"`
			Resolved  int `json:"resolved"`
		} `json:"alert_counts"`
		ImpactedServices []struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"impacted_services"`
		IsMergeable         bool          `json:"is_mergeable"`
		Importance          interface{}   `json:"importance"`
		ResolveReason       interface{}   `json:"resolve_reason"`
		IncidentsResponders []interface{} `json:"incidents_responders"`
		ResponderRequests   []interface{} `json:"responder_requests"`
		SubscriberRequests  []interface{} `json:"subscriber_requests"`
		Urgency             string        `json:"urgency"`
		ID                  string        `json:"id"`
		Type                string        `json:"type"`
		Summary             string        `json:"summary"`
		Self                string        `json:"self"`
		HTMLURL             string        `json:"html_url"`
	} `json:"incident"`
}
*/
type FullData struct {
	Incidents []FullIncidents `json:"incidents"`
	Limit     int             `json:"limit"`
	Offset    int             `json:"offset"`
	More      bool            `json:"more"`
}

type FullIncidents struct {
	IncidentNumber int           `json:"incident_number"`
	Title          string        `json:"title"`
	Description    string        `json:"description"`
	CreatedAt      time.Time     `json:"created_at"`
	Status         string        `json:"status"`
	PendingActions []interface{} `json:"pending_actions"`
	IncidentKey    string        `json:"incident_key"`
	Service        struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Summary string `json:"summary"`
		Self    string `json:"self"`
		HTMLURL string `json:"html_url"`
	} `json:"service"`
	Assignments        []interface{} `json:"assignments"`
	Acknowledgements   []interface{} `json:"acknowledgements"`
	LastStatusChangeAt time.Time     `json:"last_status_change_at"`
	LastStatusChangeBy struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Summary string `json:"summary"`
		Self    string `json:"self"`
		HTMLURL string `json:"html_url"`
	} `json:"last_status_change_by"`
	FirstTriggerLogEntry struct {
		ID        string    `json:"id"`
		Type      string    `json:"type"`
		Summary   string    `json:"summary"`
		Self      string    `json:"self"`
		HTMLURL   string    `json:"html_url"`
		CreatedAt time.Time `json:"created_at"`
		Agent     struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"agent"`
		Channel struct {
			Type        string                 `json:"type"`
			ServiceKey  string                 `json:"service_key"`
			Description string                 `json:"description"`
			IncidentKey string                 `json:"incident_key"`
			Details     map[string]interface{} `json:"details,omitempty"`
			Summary     string                 `json:"summary"`
		} `json:"channel"`
		Service struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"service"`
		Incident struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"incident"`
		Teams        []interface{} `json:"teams"`
		Contexts     []interface{} `json:"contexts"`
		EventDetails struct {
			Description string `json:"description"`
		} `json:"event_details"`
	} `json:"first_trigger_log_entry"`
	EscalationPolicy struct {
		ID              string `json:"id"`
		Type            string `json:"type"`
		Summary         string `json:"summary"`
		Self            string `json:"self"`
		HTMLURL         string `json:"html_url"`
		Name            string `json:"name"`
		EscalationRules []struct {
			ID                       string `json:"id"`
			EscalationDelayInMinutes int    `json:"escalation_delay_in_minutes"`
			Targets                  []struct {
				ID      string `json:"id"`
				Type    string `json:"type"`
				Summary string `json:"summary"`
				Self    string `json:"self"`
				HTMLURL string `json:"html_url"`
			} `json:"targets"`
		} `json:"escalation_rules"`
		Services []struct {
			ID      string `json:"id"`
			Type    string `json:"type"`
			Summary string `json:"summary"`
			Self    string `json:"self"`
			HTMLURL string `json:"html_url"`
		} `json:"services"`
		NumLoops    int           `json:"num_loops"`
		Teams       []interface{} `json:"teams"`
		Description string        `json:"description"`
		Privilege   interface{}   `json:"privilege"`
	} `json:"escalation_policy"`
	Privilege   interface{}   `json:"privilege"`
	Teams       []interface{} `json:"teams"`
	AlertCounts struct {
		All       int `json:"all"`
		Triggered int `json:"triggered"`
		Resolved  int `json:"resolved"`
	} `json:"alert_counts"`
	ImpactedServices []struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Summary string `json:"summary"`
		Self    string `json:"self"`
		HTMLURL string `json:"html_url"`
	} `json:"impacted_services"`
	IsMergeable         bool          `json:"is_mergeable"`
	Importance          interface{}   `json:"importance"`
	ResolveReason       interface{}   `json:"resolve_reason"`
	IncidentsResponders []interface{} `json:"incidents_responders"`
	ResponderRequests   []interface{} `json:"responder_requests"`
	SubscriberRequests  []interface{} `json:"subscriber_requests"`
	Urgency             string        `json:"urgency"`
	ID                  string        `json:"id"`
	Type                string        `json:"type"`
	Summary             string        `json:"summary"`
	Self                string        `json:"self"`
	HTMLURL             string        `json:"html_url"`
}
