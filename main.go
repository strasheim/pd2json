package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var PagerDutyKey string

func main() {
	sincePtr := flag.Int("t", 1, "How many hours to go back")
	pdKeyPtr := flag.String("pdkey", "", "optinal PD key if ENV as handler isn't liked")
	flag.Parse()
	pdDate := pdTime(*sincePtr)
	log.SetOutput(os.Stderr)

	PagerDutyKey = os.Getenv("PDKEY")
	if *pdKeyPtr != "" {
		PagerDutyKey = *pdKeyPtr
	}
	if PagerDutyKey == "" {
		log.Println("No PD key found. Enviroment variable PDKEY needs to be set or pdkey must be set as argument")
		os.Exit(1)
	}
	PagerDutyKey = "Token token=" + PagerDutyKey

	/* Everything is in place, let's pull down the data */

	AllServices := getAllServices(0, nil)
	service := strings.Join(AllServices, ",")

	instanceIDs := make(chan FullIncidents)
	go func() {
		getAllIncidents(pdDate, service, 0, instanceIDs)
	}()

	events := make([]FullIncidents, 0)
	for event := range instanceIDs {
		events = append(events, event)
	}

	userdata, err := json.Marshal(events)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	fmt.Println(string(userdata))

}

func getAllServices(offset int, results []string) []string {
	buffer, err := fetch("https://api.pagerduty.com/services?time_zone=UTC&sort_by=name&limit=100&offset=" + strconv.Itoa(offset))
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	response := &Services{}
	if err := json.Unmarshal(buffer, response); err != nil {
		log.Println(err)
	}

	for _, data := range response.Services {
		results = append(results, data.ID)
	}

	if response.More == true {
		offset += 100
		results = getAllServices(offset, results)
	}
	return results
}

func getAllIncidents(pdDate string, services string, offset int, instanceIDs chan FullIncidents) {
	serviceArray := strings.Split(services, ",")
	var htmlService string
	for _, service := range serviceArray {
		htmlService += "&service_ids%5B%5D=" + service
	}
	include := "&include%5B%5D=first_trigger_log_entries&include%5B%5D=escalation_policies&include%5B%5D=assignees&include%5B%5D=acknowledgers"

	buffer, err := fetch("https://api.pagerduty.com/incidents?time_zone=UTC&since=" + pdDate + "&limit=100&offset=" + strconv.Itoa(offset) + htmlService + include)
	if err != nil {
		log.Println(err)
	}
	response := &FullData{}
	if err := json.Unmarshal(buffer, response); err != nil {
		log.Println(err)
	}
	for _, data := range response.Incidents {
		instanceIDs <- data
	}

	if response.More == true {
		offset += 100
		getAllIncidents(pdDate, services, offset, instanceIDs)
	} else {
		close(instanceIDs)
	}
}

func fetch(url string) (buffer []byte, err error) {
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Accept", "application/vnd.pagerduty+json;version=2")
	req.Header.Set("Authorization", PagerDutyKey)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	buffer, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, errors.New("Calling: " + url + "resulted in: " + strconv.Itoa(resp.StatusCode))
	}
	return buffer, err
}

func pdTime(since int) string {
	sinceThen := time.Now().UTC().Add(time.Duration(-since) * time.Hour)
	year := strconv.Itoa(sinceThen.Year())
	namemonth := sinceThen.Month()
	month := strconv.Itoa(int(namemonth))
	day := strconv.Itoa(sinceThen.Day())
	hour := strconv.Itoa(sinceThen.Hour())
	minute := strconv.Itoa(sinceThen.Minute())
	return year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":00Z"
}
