### Reading out PD alerts and mangling the returned JSON

Called without arguements it will fetch results from the last hour and outputs the incidents in detail in JSON.
The paged result from PagerDuty will be unpaged.

A single JSON array with all those incidents and print that to STDOUT.

Error messages will be printed to STDERR, therefore pipeline builds are safe to assume a JSON output


### How to build 

```
go get -u gitlab.com/strasheim/pd2json
```

Once build you need to set the ENV var: PDKEY to a PD key that at least has read permissions.

----

### Default settings
* Run with a fetch timeframe of 1 hour. Can be overwritten by -t <hours> 
* The PD key will be ether read from ENV or as arg. Arg will trump ENV
* UTC as time referrence is used. PD data will be printed in as UTC

----

#### Examples
```
./pd2json 
./pd2json -t=3
./pd2json -pdkey=some_valid_key_to_read_data_from_pd
```
